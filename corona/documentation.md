# Predicting corona virus cases with genetic optimization

So first of all, (in case anyone else reads this later) I am not an expert on anything biology related, and none of my predictions are probably even remotly valid. I did this mostly to play with genetic optimization a bit more than I had in the past, and to see if I could get some valid looking results. Also, because I love playing with weird equations, and this one had some.

Now that that's out of the way, I'll provide some documentation for the program. First, a general description of how the program works:

1. User runs "basic_GUI.py"
2. User uses the sliding scales to get a somewhat ok looking match between the actual cases over time (red) and the predicted cases (green) by changing both the shift of the graph, and some parameters that define how infectious the disease is over time (the blue line)
3. User hits the "evolve" button
4. Genetic optimization magic happens (more on this later)
5. User is impressed with how accurate it was able to be (hopefully)

So now for some more specific details...

## basic_GUI.py

This file is, not suprisingly, a basic GUI for the user to play around with while also viewing the results of the parameters they select. I made it with *PySimpleGUI*. All the code in main (technically just the unindented part of the program, I didnt include an actual "main" method of course) is basic setup for the different parameters (these will be explained more later). 

Around line 46ish is the **mutate function**. This function takes a single "creature" and mutates it, changing around its "DNA". In this case, a "creature" is just a set of parameters, and the "DNA" is the actual parameters. I call it a creature and use the DNA analogy because this very accuratly analogizes what is going on in the code with how evolution works. After explaining all the methods, I will describe how they fit together. For now, knowing that the mutate function takes an array, and tweaks its values randomly a very small amount (within some space of possible values I hand chose) is enough.

Next is the **get\_best\_from\_pop** function, this takes a list of "creatures" and finds the "best" one. Here we are defining "best" as having the smallest mean square error between its predictions, and the actual historic corona virus data, using a function defined in the *algorithm.py* file.

Next we have the **evolve** function, which takes an initial population, and for *iteration* number of times, finds the best "creature" and replaces all other creatures with mutated versions of it. 

Now we can get into how the actual evolution works. So every iteration of **evolve**, only the best creature, and mutated versions of it, get left in the population array. This basically means that we are keeping only the best possible parameters that most closely fit the historic data, and we are tweaking its values a little bit too. Because we always keep the current best parameters, we ensure we are never getting worse. After tweaking the values a little bit, it is possible one of the resulting mutated parameter sets is actually better than what we started with, if that is the case, the next iteration will pick that one as the best. This is exactly how regular evolution works. A few giraffe like creatures have a short necks, and they get less food than the one that randomly happened to have a longer neck. The long necked one gets to reproduce, and its offspring might or might not have an even longer neck. Eventually we get the ridiculously long necked creatures we know today. The only difference in our code compared to normal evolution is that I am directly controlling what is considered more fit to survive, as well as controlling what the genes are. Instead of fitness being "whatever gets food", it is "whatever predicts corona virus the best". Instead of genes being "DNA" they are "parameters" for a function I will describe in more detail later on.

There are some really interesting articles and papers online about the best population sizes, amounts to evolve the creatures, wether or not to include luck, and a lot more. Honestly it does not seem like there is any sort of concensus. In the end I basically just picked meta-parameters that seemed to work decently enough. A population size of 20, around 5% mutation on average, keeping the best creature every time, and not using luck seemed to work well for me. For future research, I really want to learn what the best possible parameters are. Is it possible we could improve on our designs by looking at what natures values for these meta-parameters are (what are average population sizes for different creatures, how fast does DNA mutate, what about RNA, how many creatures go extinct compared to ones that don't, etc...)? Or could we improve on natures meta-parameters using a some sort of systematic or probabilistic search through the parameter space (which could take years, but who knows...)? There is a **lot** we as a community do not know, and even **more** I personally do not know. Doing this project gave me some insight into just how little I know, but also how much I can do even knowing very little.

## algorithm.py

This file basically handles the math and graphing of the project. I **really wanted** to do this all in one file, but after dealing with error after error for hours, I gave up and made it two files. My guess is it cannot handle making a GUI **and** graphing stuff at the same time... whatever... two files it is...

So I read online a bit about modeling diseases, but a lot of the stuff I found was either way over my head, or relied on data I simply do not have (population densities on the level of individual neighborhoods, which I could not find for myself). Then I remembered reading a while ago about how all functions could be approximated using the sigmoid function.

$S(x) = \frac 1 {1 + e^{-x}}$

![sig.png](sig.png)

It increases slowly, then ramps up faster and faster, until $x=0$, where it starts increasing less and less. It has asymptotes at $y=0$ and $y=1$. At first glance, this looks like it has nothing to do with disease spread (again, it very well might not, this was just the best insight I had after spending some time trying out different math tricks and considering different ML models), but look what happens if we negate it, scale it, and add $2$ to it...

$S(x) = 2 - \frac 2 {1 + e^{-x}}$

![negsig.png](negsig.png)

It starts off high, then decreases slowly, eventually dropping close to zero. This should, hopefully, remind you a bit of disease spread. At first, its rate of spread is high (relative to the current number of cases). One case turns to two, two to four, four to eight, etc... It is **doubling** every time step (of course the actual virus isnt *exactly* doubling, but you get the idea...). Later though, as cities take precautions, and more people are infected, it spreads slower (someone cannot have the disease spread to them if they already have it). So the rate of spreading drops down until it is below one. At that point, the disease is actually dying out (as will **hopefully** start to happen more with covid-19). Using this function as the rate of spread, it is easy to model the number of cases. Start with one case $C_0 = 1$, then at every time step, set the number of cases to the old number multiplied by the rate of spread $C_n = C_{n-1}*S_n$. So if the rate of spread is 2, the cases will go $1,2,4,8...$. When the rate of spread drops to near linear, they will go $8,9,10,11...$. Eventually, they will hit their max when the rate of spread is exactly one, then start to drop $12,12,12,11,10,9,8...$. Then they will drop exponentially faster, $4,2,1$. Eventually there will be a fraction of a single case, so we can round that down to zero. Of course this is unlikely for covid, but we can at least model when the high point will be, what it will be, and how long it will last. If we model the cases in this way, we get a nice, smooth looking curve, like the green line shown below.

![ex1.png](ex1.png)

As for the actual code of the *algorithm.py* file, it basically just implements what is described above. First we have a list of cases, which I got from a scraping program I put in a Jupyter notebook called "getting_data.ipynb". The specifics there are not honestly worth writing about, it was a whole ordeal getting the data, getting it formatted how I need it, and, although it is very important code, it is probably enough just to know it is what gets me my data on current covid-19 cases. It is all from Johns Hopkins.

Next we have the **S** function, which takes in a few parameters relating to the height of the curve, how fast it drops, the horizontal shift, etc, and computes the rate of spread for any given day given those parameters. Then we have the **get_single_score** function, which, given the parameters for the curve, computes how off it would be in predicting the cases we told it about. It uses mean square error (MSE). I chose MSE specifically after toying around with a few different error metrics because MSE captures what we are looking for pretty well. If equation A is off by 10 cases on day one, 10 on day two, and 8 on day three, that is much better than algorithm B which is off by 0 cases on day one, 1 case on day two, but 20 cases on day three. Even though B had overall less errors, I think we can all agree we would rather use model A, that's because even though A was off by more overall, its errors were much smaller than the error B had.




